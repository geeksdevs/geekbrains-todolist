//
//  MainTableViewController.swift
//  todo-list-swift
//
//  Created by Mikhail on 24.01.17.
//  Copyright © 2017 Appvilion. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {

    var arrayEvents: Array<Any>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let array = UIApplication.shared.scheduledLocalNotifications
        arrayEvents = array
        
        NotificationCenter.default.post(name: Notification.Name("NewEvent"), object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self);
    }

    func reloadTableViewWhenNewEvent() {
        arrayEvents?.removeAll()
        
        let array = UIApplication.shared.scheduledLocalNotifications
        arrayEvents = array
        self.tableView.reloadSections(IndexSet(integer: 0), with: .fade)
    }

    
// MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrayEvents?.count)!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let notification = arrayEvents?[indexPath.row] as! UILocalNotification
        var dict = notification.userInfo!
        
        cell.textLabel?.text = dict["eventInfo"] as! String?
        cell.detailTextLabel?.text = dict["eventDate"] as! String?

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let detailVC: DetailViewController = (self.storyboard?.instantiateViewController(withIdentifier: "detailView") as! DetailViewController)
        
        let notification = arrayEvents?[indexPath.row] as! UILocalNotification
        var dict = notification.userInfo!

        detailVC.eventDate = notification.fireDate
        detailVC.eventInfo = dict["eventInfo"] as! String?
        detailVC.isDetail = true
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let notification = arrayEvents?[indexPath.row] as! UILocalNotification
            UIApplication.shared.cancelLocalNotification(notification)
            self.arrayEvents?.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
 
}
