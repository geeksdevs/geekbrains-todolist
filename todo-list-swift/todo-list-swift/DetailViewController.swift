//
//  ViewController.swift
//  todo-list-swift
//
//  Created by Mikhail on 08.01.17.
//  Copyright © 2017 Appvilion. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var eventTextField: UITextField!
    @IBOutlet weak var eventDatePicker: UIDatePicker!
    @IBOutlet weak var saveButton: UIButton!
    
    public var eventDate:Date?
    public var eventInfo:String?
    public var isDetail:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (isDetail) {
            self.eventTextField.text = self.eventInfo;
            self.eventDatePicker.date = self.eventDate!;
            
            self.eventTextField.isUserInteractionEnabled = false
            self.eventDatePicker.isUserInteractionEnabled = false
            self.saveButton.isHidden = true
            
            self.perform(#selector(setDatePickerValueWithAnimation), with: nil, afterDelay: 0.5)
            
        } else {
            saveButton.addTarget(self, action: #selector(pressSaveButton), for: .touchUpInside)
            
            let handleTap = UITapGestureRecognizer(target: self, action: #selector(handleEndEditing))
            self.view .addGestureRecognizer(handleTap)
            
            eventDatePicker.minimumDate = Date()
            eventDatePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
            
            saveButton.isEnabled = false
        }
    }
    
    func setDatePickerValueWithAnimation() {
        self.eventDatePicker.setDate(self.eventDate!, animated: true)
    }
    
    func handleEndEditing() {
        self.view.endEditing(true)
        
        if ((self.eventTextField.text?.characters.count)! > 0) {
            saveButton.isEnabled = true
        }
    }

    func datePickerValueChanged() {
        eventDate = eventDatePicker.date

        NSLog("datePickerValueChanged \(eventDate)")
    }
    
    func pressSaveButton() {
        if (self.eventDate != nil) {
            if (self.eventDate?.compare(Date()) == .orderedSame) {
                self.showAlertWithMessage(message: "Дата будущего события не может совпадать с текущей датой")
                
            } else if (self.eventDate?.compare(Date()) == .orderedAscending) {
                self.showAlertWithMessage(message: "Дата будущего события не может быть ранее текущей даты")
                
            } else {
                self.setNotification()
            }

        } else {
            self.showAlertWithMessage(message: "Для сохранения события измените значение даты на более позднее")
        }
        
    }

    func setNotification() {
        let eventInfo = eventTextField.text
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm dd.MMMM.yyyy"
        let eventDateLocal = formatter.string(from: eventDate!)
        
        let dict = Dictionary(dictionaryLiteral: ("eventInfo", eventInfo!), ("eventDate", eventDateLocal))
        
        let notification = UILocalNotification()
        notification.userInfo = dict
        notification.timeZone = TimeZone.ReferenceType.default
        notification.fireDate = eventDate
        notification.alertBody = eventInfo
        notification.applicationIconBadgeNumber = 1
        notification.soundName = UILocalNotificationDefaultSoundName
        
        UIApplication.shared.scheduleLocalNotification(notification)
        
        NotificationCenter.default.post(name: Notification.Name("NewEvent"), object: nil)
        self.navigationController!.popViewController(animated: true)
    }
    
// MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.isEqual(eventTextField)) {
            if ((self.eventTextField.text?.characters.count)! > 0) {
                textField.resignFirstResponder()
                saveButton.isEnabled = true
                return true
            
            } else {
                self.showAlertWithMessage(message: "Для сохранения события введите значение в текстовое поле")
            }
        }
        return false
    }
    
// MARK: Alert Controller
    
    func showAlertWithMessage(message: String?) {
        let alertCntrllr = UIAlertController(title: "Внимание", message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertCntrllr.addAction(defaultAction)
        self.present(alertCntrllr, animated: true, completion: nil)
    }
    
    
}



