//
//  ViewController.m
//  todo-list-objc
//
//  Created by Mikhail on 08.01.17.
//  Copyright © 2017 Appvilion. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *eventTitleTextField;
@property (weak, nonatomic) IBOutlet UIDatePicker *eventDatePicker;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end


@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if (self.isDetail) {
        self.eventTitleTextField.text = self.eventInfo;
        
        self.eventTitleTextField.userInteractionEnabled = NO;
        self.eventDatePicker.userInteractionEnabled = NO;
        self.saveButton.hidden = YES;
        
        [self performSelector:@selector(setDatePickerValueWithAnimation) withObject:nil afterDelay:0.5];
        
    } else {
        [self.saveButton addTarget:self action:@selector(pressSaveButton) forControlEvents:UIControlEventTouchUpInside];
        
        UITapGestureRecognizer *handleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleEndEditing)];
        [self.view addGestureRecognizer:handleTap];
        
        self.eventDatePicker.minimumDate = [NSDate date];
        [self.eventDatePicker addTarget:self action:@selector(datePickerValueChanged) forControlEvents:UIControlEventValueChanged];
        self.saveButton.enabled = NO;

    }
}

- (void)setDatePickerValueWithAnimation {
    [self.eventDatePicker setDate:self.eventDate animated:YES];
}

- (void)handleEndEditing {
    [self.view endEditing:YES];
    
    if (self.eventTitleTextField.text.length > 0) {
        self.saveButton.enabled = YES;
    }
}

- (void)datePickerValueChanged {
    self.eventDate = self.eventDatePicker.date;
    
    NSLog(@"datePickerValueChanged %@", self.eventDate);
}

- (void)pressSaveButton {
    if (self.eventDate) {
        
        if ([self.eventDate compare:[NSDate date]] == NSOrderedSame) {
            [self showAlertWithMessage:@"Дата будущего события не может совпадать с текущей датой"];
        
        } else if ([self.eventDate compare:[NSDate date]] == NSOrderedAscending) {
            [self showAlertWithMessage:@"Дата будущего события не может быть ранее текущей даты"];
        
        } else {
            [self setNotification];
        }
    
    } else {
         [self showAlertWithMessage:@"Для сохранения события измените значение даты на более позднее"];
    }
}

- (void)setNotification {
    NSString *eventInfo = self.eventTitleTextField.text;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"HH:mm dd.MMMM.yyyy";
    
    NSString *eventDate = [formatter stringFromDate:self.eventDate];
    
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:eventInfo, @"eventInfo", eventDate, @"eventDate", nil];
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.userInfo = dict;
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.fireDate = self.eventDate;
    notification.alertBody = eventInfo;
    notification.applicationIconBadgeNumber = 1;
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewEvent" object:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.eventTitleTextField]) {
        
        if (textField.text.length > 0) {
            [textField resignFirstResponder];
            self.saveButton.enabled = YES;
            return YES;
        
        } else {
            [self showAlertWithMessage:@"Для сохранения события введите значение в текстовое поле"];
        }
    }
    
    return NO;
}

- (void)showAlertWithMessage:(NSString *)message {
    UIAlertController *alertCntrllr = [UIAlertController alertControllerWithTitle:@"Внимание!" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    
    [alertCntrllr addAction:defaultAction];
    [self presentViewController:alertCntrllr animated:YES completion:nil];
}

@end
