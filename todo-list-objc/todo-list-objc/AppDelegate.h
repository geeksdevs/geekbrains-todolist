//
//  AppDelegate.h
//  todo-list-objc
//
//  Created by Mikhail on 08.01.17.
//  Copyright © 2017 Appvilion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

