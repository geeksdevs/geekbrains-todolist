//
//  ViewController.h
//  todo-list-objc
//
//  Created by Mikhail on 08.01.17.
//  Copyright © 2017 Appvilion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) NSDate *eventDate;
@property (strong, nonatomic) NSString *eventInfo;
@property (assign, nonatomic) BOOL isDetail;

@end

