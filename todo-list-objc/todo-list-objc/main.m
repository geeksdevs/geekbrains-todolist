//
//  main.m
//  todo-list-objc
//
//  Created by Mikhail on 08.01.17.
//  Copyright © 2017 Appvilion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
